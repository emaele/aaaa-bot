package utility

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"
)

// GetRandomPic chose a random image from folder
func GetRandomPic(folder string) (path string) {
	currDir, err := ioutil.ReadDir(folder)
	if err != nil {
		log.Fatal(err)
	}

	var justFiles []os.FileInfo
	for _, elem := range currDir {
		if elem.IsDir() {
			continue
		}
		justFiles = append(justFiles, elem)
	}
	rand.Seed(time.Now().UnixNano())
	randomInt := rand.Intn(len(justFiles))
	file := justFiles[randomInt]

	path = folder + file.Name()
	return
}
