package main

import (
	"flag"
	"log"
	"strings"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	conf "gitlab.com/emaele/aaaa-bot/config"
	"gitlab.com/emaele/aaaa-bot/utility"
)

var (
	config         conf.Config
	err            error
	configFilePath string
	debug          bool
)

func main() {

	setCLIParams()

	config, err = conf.ReadConfig(configFilePath)
	if err != nil {
		log.Panic(err)
	}

	bot, err := tgbotapi.NewBotAPI(config.TelegramTokenBot)
	if err != nil {
		log.Panic(err)
	}
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = debug

	log.Printf("Authorized on account %s", bot.Self.UserName)

	for update := range updates {
		if update.Message != nil {
			go mainBot(bot, update)
		}
	}
}

func mainBot(bot *tgbotapi.BotAPI, update tgbotapi.Update) {

	if strings.Contains(update.Message.Text, config.Trigger) {

		pic := utility.GetRandomPic(config.Folder)
		msg := tgbotapi.NewPhotoUpload(update.Message.Chat.ID, pic)
		bot.Send(msg)
	}

}

func setCLIParams() {
	flag.BoolVar(&debug, "debug", false, "activate all the debug features")
	flag.StringVar(&configFilePath, "config", "./config.toml", "configuration file path")
	flag.Parse()
}
